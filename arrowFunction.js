//-------------------------------------------------------
// No.1

const golden = () => {
console.log("this is golden!");
}
golden();

//-------------------------------------------------------
// No.2

firstName = 'William'
lastName = 'Imoh'
 
const kata = `${firstName} ${lastName}`

console.log(kata)

//-------------------------------------------------------
// No.3

var newObject = {
  namaDepan: "Harry",
  namaBelakang: "Potter Holt",
  tujuan: "Hogwarts React Conf",
  cakupan: "Deve-wizard Avocado",
  pengucapan: "Vimulus Renderus!!!"
};

const {namaDepan, namaBelakang, tujuan, cakupan, pengucapan} = newObject

console.log(namaDepan, namaBelakang, tujuan, cakupan, pengucapan)

//------------------------------------------------------------------------
// No.4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east,]
console.log(combined)

//-------------------------------------------------------------------------
// No.5

const planet = 'earth'

const view   = 'glass'

var text = `ini adalah ${planet} terlihat seperti ${view}`

console.log(text)


